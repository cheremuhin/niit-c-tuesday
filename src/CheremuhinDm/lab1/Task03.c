/*��������� ����������� ������� � ������� � �������*/
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>

void clean_stdin()
{
	int c;
	do
	{
		c = getchar();
	} while (c != '\n' && c != EOF);
}

int main()
{
	char Answ[] = "Answer is ", lit = "D";
	float ent_number = .0f, coeff = 57.2958f, result = .0f;

	puts("Please enter the value in degrees or radians.\nUse R if it is a radian or D if it is a degree.\n\nFor example: 12.34R\n\n");
	while (1)
	{
		scanf("%f%c", &ent_number, &lit);
		if (lit == "D" || lit == "R")
			break;
		else
		{
			puts("Input error!");
			clean_stdin();			
		}
	}		
	if (lit == 'D')
	{
		result = ent_number / coeff;
		printf("%s%.2f R\n", Answ, result);
	}
	else if (lit == 'R')
	{
		result = ent_number * coeff;
		printf("%s%.2f D\n", Answ, result);
	}
	
	return 0;
}