#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

struct MAZE { // Через эту структуру работаем с параметрами лабиринта.
	char**final; // Адрес дин. массива в который генерируется лабиринт.
	int width; // Ширина лабиринта.
	int height;	// Высота лабиринта.
	int vert; // Вертикальная позиция X.
	int hor; // Горизонтальная позиция X.
};

typedef struct MAZE TMAZE;

int makeLattice(TMAZE * maze); // Создает решетку-заготовку лабиринта.
int getNumber(char quote[], int min, int max); // Выдает запрос пользователю. Принимает целое число в указанном диапазоне.
void arrPrinterToFile(TMAZE * maze); // Печатает содержимое дин. массива в файл.
void arrPrinter(TMAZE * maze); // Печатает содержимое дин. массива в ком. строку.
void TheMole(TMAZE * maze); // Рекурсивно "роет" проходы в сгенерированной решетке-заготовке лабиринта. Метит пройденные клетки звёздочками.
void signRemove(TMAZE * maze, char sign);// Очистка лабиринта от 'знака'.
int Pathfinder(TMAZE * maze, int vert, int horiz); //Ищет выход, отмечая пройденные и тупиковые клетки.
void Positioning (TMAZE * maze); //Позиционирование выхода и X.