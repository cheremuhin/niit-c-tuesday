// Генерация лабиринта и нахождение выхода из него.
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <windows.h>
#include "task07.h"
#define W 75
#define H 100

int makeLattice(); 
void arrPrinterToFile();
int getNumber();
void arrPrinter();
void signRemove();
int Pathfinder();


int main() {
	int i = 0;
	char quotes[3][100] = {"Please, enter the width of the maze", "Please, enter the height of the maze", 
		"\n>>> Please, maximize the  the console window!\n\n"};	
	TMAZE *maze;
	srand(time(NULL));
	maze = (TMAZE*)calloc(1, sizeof(TMAZE));	
	printf("%s", quotes[2]);
	maze->width = getNumber(quotes[0], 6, W);
	maze->height = getNumber(quotes[1],6, H);
	if (maze->width%2 != 1) maze->width = maze->width + 1;
	if (maze->height%2 != 1) maze->height = maze->height + 1;
	maze->final = (char**)calloc(maze->height+1, sizeof(char*));
	for (i = 0; i < maze->height; i++){
		maze->final[i] = (char*)calloc((maze->width+1), sizeof(char));
		memset(maze->final[i], ' ', maze->width * sizeof(char));
	}	
	makeLattice(maze);	
	TheMole(maze, 1, 1);
	signRemove(maze, '*');
	puts("\nEmpty maze:\n");
	arrPrinter(maze);
	Sleep(1000);
	puts("\nPositioning:\n");
	Positioning (maze);
	arrPrinter(maze);
	Sleep(1000);
	puts("\nSearching:\n");	
	Pathfinder(maze, maze->vert, maze->hor);
	maze->final[maze->vert][maze->hor] = 'X';
	arrPrinter(maze);
	Sleep(1000);
	signRemove(maze, '0');	
	puts("\nRoute:\n");
	arrPrinter(maze);	
	for(i = 0; i <= maze->height; i++) free(maze->final[i]); // Очистка памяти от дин.массива.
	free (maze->final);
	return 0;
}