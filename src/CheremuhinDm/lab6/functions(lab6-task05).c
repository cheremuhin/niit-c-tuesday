#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "task05.h"

void clean_stdin()
{
	int c;
	do {
		c = getchar();
	} while (c != '\n' && c != EOF);
}

int getNumber(int min, int max){
	int number = 0;
	while (1){
				printf("Please, enter the number of iterations(from %d to %d):\n", min, max);
				scanf("%d", &number);
				if (number < min || number > max){
					puts("Input error!\n");
					clean_stdin();
				}
				else break;
			}
return number;
}

void numFib(struct FIBONACCI*fibonacci) {
	fibonacci->result = fibonacci->SB2 + fibonacci->SB;
	fibonacci->SB2 = fibonacci->SB;
	fibonacci->SB = fibonacci->result;
	fibonacci->t = clock() - fibonacci->t;
	printf("The result is %.0f. Used time is %.5f\n", fibonacci->result, fibonacci->t);
	fibonacci->count += 1;
	if(fibonacci->count<fibonacci->number) numFib(fibonacci);
}