#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "task07.h"

void clean_stdin() { // Очистка стандартного потока ввода(stdin).
	int c;
	do {
		c = getchar();
	} while (c != '\n' && c != EOF);
}

int getNumber(char quote[], int min, int max){ // Выдает запрос пользователю. Принимает целое число в указанном диапазоне.
	int number = 0;
	while (1){
		printf("%s (from %d to %d):\n", quote, min, max);
		scanf("%d", &number);
		if (number < min || number > max){
			puts("Input error!\n");
			clean_stdin();
		}
		else break;
	}
return number;
}

int makeLattice(TMAZE * maze) { // Создает решетку-заготовку лабиринта.
	int i = 0, j = 0;	
	for (i = 0; i < maze->height;i++) {
		for (j = 0; j < maze->width;j++) {
			if (i == maze->height - 1 || j == maze->width - 1 || i%2 == 0 || j%2 == 0) maze->final[i][j] = '#';			
		}	
	}
return 0;
}

void arrPrinterToFile(TMAZE * maze){ // Печатает содержимое дин. массива в файл.
	int i = 0, j = 0;
	FILE*file;
	file = fopen("Maze.txt", "a");
	for(i = 0; i < maze->height; i++) {
		for(j = 0; j < maze->width; j++) putc(maze->final[i][j], file);
		putc('\n', file);
	}
	fclose(file);
	// system("C:\\windows\\notepad.exe Maze.txt");	// Для открытия файла после печати.	
}

void arrPrinter(TMAZE * maze){ // Печатает содержимое дин. массива в строку.
	int i = 0, j = 0;
	for(i = 0; i < maze->height; i++) {
		for(j = 0; j < maze->width; j++) putchar(maze->final[i][j]);
		putchar('\n');
	}
}

void TheMole(TMAZE * maze, int vert, int horiz) { // Рекурсивно "роет" проходы в сгенерированной решетке-заготовке лабиринта. 
                           						 // Метит пройденные клетки звёздочками, а тупики - нулями.
	int rnum = 0, arr[5] = {0}, i = 0, count = 0;
	maze->final[vert][horiz] = '*';
	while (count != 4) {
		rnum = rand()%4 + 1;
		for (i = 0; i < 4; i++) {		 
			if (arr[i] == rnum) break;
			if (arr[i] == 0) { 
				arr[i] = rnum;
				count++;
				break;
			}
		}	
	}	
	for (i = 0; i < 4; i++) {
		if (arr[i] == 1 && vert > 1 && maze->final[vert-2][horiz] != '*') {
			maze->final[vert-1][horiz] = '*';
			TheMole(maze, vert-2, horiz);
			continue;
		}
		if (arr[i] == 2 && horiz < maze->width - 2 && maze->final[vert][horiz+2] != '*') {
			maze->final[vert][horiz+1] = '*';
			TheMole(maze, vert, horiz+2);
			continue;
		}
		if (arr[i] == 3 && vert < maze->height - 2 && maze->final[vert+2][horiz] != '*') {
			maze->final[vert+1][horiz] = '*';
			TheMole(maze, vert+2, horiz);
			continue;
		}
		if (arr[i] == 4 && horiz > 1 && maze->final[vert][horiz-2] != '*') {
			maze->final[vert][horiz-1] = '*';
			TheMole(maze, vert, horiz-2);
			continue;
		}
	}	
}

void signRemove(TMAZE * maze, char sign) { // Очистка лабиринта от 'знака'.
	int i = 0, j = 0;
	for(i = 0; i < maze->height; i++) 
		for(j = 0; j < maze->width; j++)
			if(maze->final[i][j] == sign) maze->final[i][j] = ' ';		
	}

void Positioning (TMAZE * maze) { //Позиционирование выхода и X.
	int dem = 0, pos_e = 0, pos_x = 0;
	dem = rand()%4 + 1;
	if (dem == 1) {
		while(1) {
			pos_e = rand() % (maze->height-3) + 1;
			if (maze->final[pos_e][1]==' ') { 
				maze->final[pos_e][0] = ' ';
				break;
			}
		}
		while(1) {
			pos_x = rand() % (maze->height-3) + 1;
			if (maze->final[pos_x][maze->width-2]==' ') {
				maze->final[pos_x][maze->width-2] = 'X';
				maze->vert = pos_x;
				maze->hor = maze->width-2;
				break;			
			}
		}
	}
	if (dem == 2) {
		while(1) {
			pos_e = rand() % (maze->width-3) + 1;
			if (maze->final[1][pos_e] == ' ') {
				maze->final[0][pos_e] = ' ';
				break;
			}
		}
		while(1) {
			pos_x = rand() % (maze->width-3) + 1;
			if (maze->final[maze->height-2][pos_x]==' ') {
				maze->final[maze->height-2][pos_x]= 'X';
				maze->vert = maze->height-2;
				maze->hor = pos_x;
				break;
			}
		}
	}
	if (dem == 3) {
		while(1) {
			pos_e = rand() % (maze->height-3) + 1;
			if (maze->final[pos_e][maze->width-2]==' ') {
				maze->final[pos_e][maze->width-1] = ' ';
				break;
			}
		}
		while(1) {
			pos_x = rand() % (maze->height-3) + 1;
			if (maze->final[pos_x][1]==' ') {
				maze->final[pos_x][1] = 'X';
				maze->vert = pos_x;
				maze->hor = 1;
				break;
			}
		}
	}
	if (dem == 4) {
		while(1) {
			pos_e = rand() % (maze->width-3) + 1;
			if (maze->final[maze->height-2][pos_e]==' ') {
				maze->final[maze->height-1][pos_e] = ' ';
				break;
			}
		}
		while(1) {
			pos_x = rand() % (maze->height-3) + 1;
			if (maze->final[1][pos_x]==' ') {
				maze->final[1][pos_x] = 'X';
				maze->vert = 1;
				maze->hor = pos_x;
				break;
			}
		}
	}
}

int Pathfinder(TMAZE * maze, int vert, int horiz) { //Ищет выход, отмечая пройденные и тупиковые клетки.

	int rnum = 0, arr[5] = {0}, count = 0, i = 0;
	char sign = '0';
	
    if (vert == 0 || horiz == 0 || vert == maze->height-1 || horiz == maze->width-1) {
		maze->final[vert][horiz] = '*';
		return 1;
	}
	while (count != 4) {
		rnum = rand()%4 + 1;
		for (i = 0; i < 4; i++) {		 
			if (arr[i] == rnum) break;
			if (arr[i] == 0) { 
				arr[i] = rnum;
				count++;
				break;
			}
		}	
	}
	count = 0;	
	for (i = 0; i < 4; i++) {
	if (arr[i] == 1 && maze->final[vert - 1][horiz] != '#' 
		&& maze->final[vert - 1][horiz] != sign && maze->final[vert - 1][horiz] != '*') {
		maze->final[vert][horiz] = '*';
		if(Pathfinder(maze, vert - 1, horiz)==1) {
			count++;
			return 1;
		}
		else count;
	}
	if (arr[i] == 2 && maze->final[vert][horiz + 1] != '#' 
		&& maze->final[vert][horiz + 1] != sign && maze->final[vert][horiz + 1] != '*') {
		maze->final[vert][horiz] = '*';		
		if(Pathfinder(maze, vert, horiz + 1)==1) {
			count++;
			return 1;
		}
		else count;
	}
	if (arr[i] == 3 && maze->final[vert + 1][horiz] != '#' 
		&& maze->final[vert + 1][horiz] != sign && maze->final[vert + 1][horiz] != '*') {
		maze->final[vert][horiz] = '*';		
		if(Pathfinder(maze, vert + 1, horiz)==1) {
			count++;
			return 1;
		}
		else count;
	}
	if (arr[i] == 4 && maze->final[vert][horiz - 1] != '#' 
		&& maze->final[vert][horiz - 1] != sign && maze->final[vert][horiz - 1] != '*') {
		maze->final[vert][horiz] = '*';		
		if(Pathfinder(maze, vert, horiz - 1)==1) {
			count++;
			return 1;
		}
		else count;
	}
}
	if (count == 0) maze->final[vert][horiz] = '0';
	return 0;
}	