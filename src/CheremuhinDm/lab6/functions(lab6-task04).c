#include <stdio.h>
#include <stdlib.h>

typedef unsigned long long ULL;

void clean_stdin()
{
	int c;
	do {
		c = getchar();
	} while (c != '\n' && c != EOF);
}

int intPower(int a, int b){ // Возводит нат. число в степень. a - нат. число, b - степень числа
	int i, rez = 1;
	if (b == 0) return 1;
	if (b == 1) return a;
	if (b < 0) { // данную ветку оставляю для возможного переделывания этой функции в тип float и т.п.
		for (i=1; i<=abs(b); i++) { // при отриц. b будет возвращать как при b == 0.
			rez *= a;
		}
		rez = 1/rez;
		return rez;
	} 
	if (b > 1) {
	    for(i = 1; i <= b;) {
	        rez *= a;
			i++;
	    }
	    return rez;
	}
}

int getNumber(int min, int max){
	int number = 0;
	while (1){
				printf("Please, enter the number of iterations(from %d to %d):\n", min, max);
				scanf("%d", &number);
				if (number < min || number > max){
					puts("Input error!\n");
					clean_stdin();
				}
				else break;
			}
return number;
}