// Измерение времени вычисления N-го члена ряда Фибоначчи.
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "task05.h"
#define MIN 1
#define MAX 100

int main() {

	struct FIBONACCI *fibonacci = NULL;
	fibonacci = (struct FIBONACCI*)calloc(1, sizeof(struct FIBONACCI));
	fibonacci->number = getNumber(MIN, MAX);
	fibonacci->SB = 1;
	fibonacci->SB2 = 0;
	fibonacci->t = clock();
	numFib(fibonacci);
	return 0;
}