#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef unsigned long long ULL;

struct FIBONACCI {
	double SB;
	double SB2;
	double result;
	short number;
	short count; 
	clock_t t;
};

void clean_stdin();
int getNumber(int min, int max);
void numFib(struct FIBONACCI*fibonacci);
