//Написать программу, которая находит в диапазоне целых чисел от 2 до 1000000 число,
// формирующее самую длинную последовательность Коллатца.
#include <stdio.h>
#include <stdlib.h>

struct COLLATZ{ // В структуре находятся счетчик итераций, изначальное число, 
	int count; // промежуточный результат, макс. кол-во итераций, число с максимальным кол-м итераций.
	int number;
	int result;
	int maxcnt;
	int maxnmb;
};

void funCollatz(struct COLLATZ *collatz){
	if (collatz->result % 2 == 0) {
		collatz->result = collatz->result/2;
		collatz->count += 1;
	}
	else {
		collatz->result = collatz->result * 3 + 1;
		collatz->count += 1;		
	}
	if (collatz->result <= 1) {
		if (collatz->count > collatz->maxcnt){
			collatz->maxcnt = collatz->count;
			collatz->maxnmb = collatz->number;
		}
	}
	else funCollatz(collatz);	
}

int main() {
	int i = 0;
	struct COLLATZ *collatz;
	collatz = (struct COLLATZ*)calloc(1, sizeof(struct COLLATZ));
	for(i = 2; i <= 1000000; i++) {
		collatz->count = 0;
		collatz->number = i;
		collatz->result = i;
		funCollatz(collatz);
	}
	printf("The number is %d. \nThe number of iterations is %d\n", collatz->maxnmb, collatz->maxcnt);
	return 0;
}