// Декомпрессор Хаффмана.
// Автор работы: Черёмухин Дмитрий
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#define N 1000000 // Величина буфера 101
#define L 11 // Колличество статичных байт в начале заголовка, до начала таблицы встречаемости
#define MaxC 100 // Максимальная длина кода символа. При вырожденном дереве может потребоваться увеличение.

typedef unsigned char uc;

struct SYM {
	short ch;
	unsigned int freq;
	char code[256];
	struct SYM *left;
	struct SYM *right;
};

typedef struct SYM TSYM;
typedef TSYM * PSYM;

PSYM titleParser(FILE* fp, int* c, int* fsize, char* tail) { // Парсит заголовок сжатого файла.
	int i = 1, size = 10;
	char buf[10] = {0};
	PSYM sym_arr = NULL;
	if(!fp) {
		perror("ERROR");
		getch();
		exit(1);
	}
	fseek( fp, 0, SEEK_END); // Проверка на нулевой(пустой файл).
	size = (ftell(fp)); 
	if (size == 0) {
		puts("There is no content in the source file!");
		exit(2);
	}
	rewind(fp);
	if( fread( buf, sizeof(char), 4, fp) && strcmp( buf, "dstc") == 0) { // Считывает подпись
		fread( tail, sizeof(char), 1, fp); // Считывает величину "хвоста"
		fread( fsize, sizeof(int), 1, fp); // Размер исходного файла в байтах
		fread( c, sizeof(short), 1, fp); // Число уникальных символов в таблице встречаемости.
		sym_arr = (PSYM)calloc((*c), sizeof(TSYM));
		for (i = 0; i < (*c); i++) { // Читает таблицу встречаемости.
			fread(&sym_arr[i].ch, sizeof(uc), 1, fp);
			fread(&sym_arr[i].freq, sizeof(unsigned int), 1, fp);
		}
	}
	else {
		puts("Invalid file type!"); // При несовпадении подписи завершается с ошибкой.
		exit(1);
	}
	return sym_arr;
}

PSYM* ppArr(PSYM tmp, short c) { 						// Переводит таблицу встречаемости в массив указателей на указатели на структуру, 
	int i = 0;											// для передачи в ф-ю построения H-дерева
	PSYM* pp = (PSYM*)calloc(c+1, sizeof(PSYM));		
	for(i=0; i<c; i++){
		pp[i] = &tmp[i];
		pp[i]->left=pp[i]->right=NULL;
	}
	return pp; 
}

PSYM buildTree(PSYM psym[], int s) { // Построитель H-дерева
	short i = 0, n;
	PSYM tmp = NULL;
	struct SYM *temp=(struct SYM*)malloc(sizeof(struct SYM));
	temp->ch = -5;
	temp->freq=psym[s-2]->freq+psym[s-1]->freq;
	temp->left=psym[s-1];
	temp->right=psym[s-2];
	temp->code[0]=0;
	if(s==2) 
	return temp;
	psym[s-1] = NULL;
	psym[s-2] = temp;
	n = s - 2;
 	for(i=0;i<n && psym[n-i]->freq >= psym[n-i-1]->freq;i++){
		tmp = psym[n-i];
		psym[n-i] = psym[n-i-1];
		psym[n-i-1] = tmp;		
	}
	return buildTree(psym,s-1);
}

void makeCodes(PSYM root) { // Кодер
	if(root->left != NULL) {
		strcpy(root->left->code,root->code);
		strcat(root->left->code,"0");
		makeCodes(root->left);
	}
	if(root->right != NULL) {
		strcpy(root->right->code,root->code);
		strcat(root->right->code,"1");
		makeCodes(root->right);
	}
}

void parseByte(unsigned char B, char* Out) { // Парсер байтов
	int i = 0;	
	for( ; i<8; i++) Out[7-i] = ((B>>i)&1)+'0';	
}

void printTree(PSYM node) { // Печать дерева. Для диагностики.
	if(node->left) printTree(node->left);	
	if(node->ch != -5) printf("Number: %d  Char: %c  code: %s \n", node->ch, node->ch, node->code);
	if(node->right) printTree(node->right);
}

void findCode(PSYM node, PSYM* res, char* buf, int* j) { // Функция поиска кодовой последовательности по H-дереву
	if(node->left != NULL && *buf == '0' && *res == NULL) {
		(*j)++;
		findCode(node->left, res, (buf+1), j);
	}
	else if(node->right != NULL && *buf == '1' && *res == NULL) {
		(*j)++;
		findCode(node->right, res, (buf+1), j);
	}
			else if (*res == NULL) *res = node;				
}	

void Decompressor(FILE*fp_in, FILE*fp_out, PSYM node, char tail, int c) { // Собственно декомпрессор.
	unsigned int i = 0, j = 0, length = 0, count = 0, size = 0, inner_tail = 0;
	uc buf = 0;	
	PSYM res = NULL;
	char*pArr = ( char*)calloc( N, sizeof( char));
	fseek(fp_in,0,SEEK_END); 
	size = (ftell(fp_in)); 
	fseek(fp_in, (count = L+5*c), SEEK_SET);	// Выставление каретки на конец заголовка
	while(1) {										// При изменении заголовка - изменить.
		fread( &buf, sizeof(unsigned char), 1, fp_in);
		count++;
		if (!feof(fp_in)) {
			parseByte(buf, &pArr[i]);
			i+=8;
		}
		if (i >= (N-MaxC*2) || count >= size) { // Ограничение числа бит записанных в массив 101.
			length = strlen(pArr);
			if(count>=size) {
				for( i = 0; i < tail; i++) pArr[length-i-1] = 0;				
			}			
			for( j = 0; (j < length - MaxC) || count>=size; ) { 	// Цикл предела чтения для функции findCode.
				findCode( node, &res, &pArr[j], &j);				
				fwrite(&(res->ch), sizeof(uc),1,fp_out);
				res = NULL;	
				if (pArr[j+1] == 0) break;			
			}			
			if (pArr[j+1] == 0) break;
			for(i = 0; j < length; i++, j++) pArr[i] = pArr[j];			
			if (i > 0) {				
				for((j = i+1); j < (length+2); j++) pArr[j] = 0;				
			}			
		}	
	}
	fclose(fp_out);
	fclose(fp_in);
}

int main(int argc, char* argv[]) {
	clock_t t1 = clock();
	PSYM table = NULL, h_tree = NULL;
	FILE*fp_in = NULL;
	FILE*fp_out = NULL;
	char tail = 0;
	int c = 0, fsize = 0;
	if (!argv[1] || !argv[2]) {
		printf("\nHello, dear user! This short manual is for you!\n\n"	
		"To use this decompressor you should enter the following commands:\n"
		"<disk>:/<dir>/decompressor.exe <disk>:/<dir>/<fname.ext.comp> " 
		"<disk>:/<dir>/<fname.ext>\n\n"
		"Complaints and suggestions: d.st.cheremuhin@gmail.com\n\nPress any key.\n\n");
		getch();
		exit (1);
	}
	fp_in = fopen(argv[1], "rb");
	fp_out = fopen(argv[2], "wb");
	table = titleParser( fp_in, &c, &fsize, &tail);
	h_tree = buildTree(ppArr(table, c), c);
	makeCodes(h_tree);
	//printTree(h_tree); // Оставил для диагностики.
	Decompressor(fp_in, fp_out, h_tree, tail, c);
	printf("\nInput file: %s\nOutput file: %s\nDecompression completed.\nElapsed time: %.3f\n", 
		argv[1], argv[2], ((double)(clock()-t1)/CLOCKS_PER_SEC));
	return 0;
}
