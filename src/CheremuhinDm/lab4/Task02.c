//Написать программу, которая с помощью массива указателей выводит слова
//строки в обратном порядке

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "d_funct.c"
#define N 200

int main()
{
	int i = 0, j = 0, tmp = 0;
	char text[N] = { 0 };
	char *p_arr[N] = { 0 };
	puts("Type your text: ");
	if (*fgets(text, N, stdin) != '\n') {
		for (i = 0; i < strlen(text); i++) {
			if (text[i] != ' ' && text[i] != '\n') {
				p_arr[j] = &text[i];
				j++;
			}
			while (text[i] != ' ' && text[i] != '\n') i++;
		}
	}
	i = 0;
	while (i < j / 2) {
		charChanger(p_arr+i, p_arr+j - i - 1); 
		i++;
	}	
	tmp = j;
	for (i = 0; i < tmp; i++) {
		j = 0;
		while (*(p_arr[i]+j) != '\n' && *(p_arr[i]+j) != ' ') {
			putchar(*(p_arr[i]+j) );			
			j++;
		}		
		putchar(' ');
	}
	putchar('\n');
	return 0;
}