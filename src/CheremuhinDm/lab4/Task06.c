//Самый молодой и самый старый родственник
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>

int main() {
	char names[20][20] = { 0 };
	int ages[20] = { 0 }, num = 0, i = 0, min = 100, max = 0;
	char*young = 0;
	char*old = 0;
	//Ввод данных
	while (1) {
		puts("Number of relatives(1-20): ");
		scanf("%d", &num);
		if (num >= 1 && num <= 20)
			break;
		else {
			puts("Input error!");
			clean_stdin();
		}
	}	
	for (i = 0; i < num; i++) {
		printf("Name number %d: ", i+1);
		clean_stdin();
		fgets(names[i], 20, stdin);
		while (1) {
			printf("Age: ");
			scanf("%d", &ages[i]);
			if (ages[i] >= 1 && ages[i] <= 100)
				break;
			else {
				puts("Input error!");
				clean_stdin();
			}
		}
		if (min > ages[i]) {
			min = ages[i];
			young = names[i];
		}
		if (max < ages[i]) {
			max = ages[i];
			old = names[i];
		}
	}	
	printf("The 'young' is %sThe 'old' is %s", young, old);
	return 0;
}