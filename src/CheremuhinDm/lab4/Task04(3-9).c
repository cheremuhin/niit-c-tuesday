// Вывод длиннейшей последовательности символов
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#define N 1000

int main() {
	int i = 0, count = 1, max = 0;
	int*p_count = 0;
	int*p_max = 0;
	char text[N] = { 0 }, sym = 0;
	char*p_arr[N] = { 0 };
	char*p_sym = 0;
	// Ввод данных
	puts("Type your text: ");
	fgets(text, 999, stdin);
	putchar('\n');
	for (i = 0; i < strlen(text); i++) {
		p_arr[i] = &text[i];
	}
	if (*p_arr[0] == '\n') {
		puts("No words");
		return 0;
	}
	p_count = &count;
	p_max = &max;
	p_sym = &sym;
	// Определение длиннейшей последовательности
	i = 0;
	do {
		for (; *(p_arr[i]) == *(p_arr[i + 1]); i++) 
			(*p_count)++;		
		if (*p_count > *p_max) {
			*p_sym = *p_arr[i];
			*p_max = *p_count;			
		}
		*p_count = 1;
		i++;
	} while (i < strlen(*p_arr)-1);
	putchar('\n\n');
	// Вывод ответа	
	printf("Symbol '%c' was repeated %d times.\n", sym, max);
	return 0;
}