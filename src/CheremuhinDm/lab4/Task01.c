// Вывод строк в порядке возрастания длины.
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define N 100
#define M 100

int comp(const void *a, const void *b) {
	return (strlen(*(char**)a) - strlen(*(char**)b));
}

int main()
{
	int i = 0, count = 0, len = 0;
	char text[N][M] = { 0 };
	char *p_arr[N] = { 0 };
	// Ввод данных
	FILE*in = fopen("Input.txt", "rt");
	FILE*out = fopen("Output.txt", "wt");
	if (in == 0 || out == 0) {
		perror("File:");
		return 1;
	}
	while (fgets(text[count], N, in) != '\n'){
		p_arr[count] = text[count];
		len = strlen(p_arr[count]);	
		if ((p_arr[count][len-1]) != '\n' && len != 0)p_arr[count][len] = '\n';
		if (text[count][0] == '\0') break;
		count++;		
	}		 
	// Cортировка и вывод
	qsort(p_arr, count, sizeof(char*), comp);
	for (i = 0; i < count; i++) {
			fputs(*(p_arr+i), out);
			fflush(out);	
	}		
	return 0;
}