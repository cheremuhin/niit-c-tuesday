/*Написать программу, которая строит таблицу встречаемости символов для 
произвольного файла, имя которого задаётся в командной строке. Программа должна выводить на экран 
 таблицу встречаемости, отсортированную по убыванию частоты*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define N 20 // Максимальная длина слова.

struct NODE {
	unsigned char word[N];
	int count;
	struct NODE * left;
	struct NODE * right;
};

typedef struct NODE TNODE;
typedef TNODE * PNODE;

short Validator(short var) {
	if (var > 64 && var < 90 || var > 96 && var < 123) // Диапазон настраивается под потребности.
		return 1; 
	else return 0;
}

PNODE workTree(PNODE node, unsigned char* buf, short mdf) {
	short i = 0;
	if (mdf == 1 && node == NULL) {
		node = (PNODE)calloc(1,sizeof(TNODE));
		for(i=0; i<strlen(buf); i++) node->word[i] = buf[i];
		node->left=node->right=NULL;
	}	
	if (mdf == 2 && strcmp (node->word, buf)==0) 
		node->count++;
	if (strcmp (node->word, buf)>0 && (mdf == 1 || (mdf == 2 && node->left != NULL))) 
		node->left = workTree(node->left, buf, mdf);
			else if (strcmp (node->word, buf)<0 && (mdf == 1 || (mdf == 2 && node->right != NULL))) 
				node->right = workTree(node->right, buf, mdf);			
	return node;
}

void printTree(PNODE node) {
	if(node->left) printTree(node->left);	
	printf("Word: %s  Number: %d \n", node->word, node->count);
	if(node->right) printTree(node->right);
}

PNODE Executor(FILE*fp, PNODE (*p)(PNODE, unsigned char*, short), PNODE tmp, short mdf) {
	short i = 0, c = 10;
	unsigned char buf[N] = {0};
	if(!fp) {
		perror("ERROR");
		getch();
		exit(1);
	}
	while(c!=EOF && c!= 0) {		
		for( i=0; i<N; i++) {
		if (Validator(c = fgetc(fp)) == 1) buf[i] = c;
		else break;		
		}		
		if( buf[0] != 0) 
			tmp = (*p)(tmp, buf, mdf);
		while( i>=0 && i--) buf[i] = 0;
	}
	return tmp;
}

int main( int argc, char*argv[]) {	
	PNODE tmp = NULL;
	PNODE (*p_workTree)( PNODE, unsigned char*,short) = workTree;
	if (!argv[1] || !argv[2]) {
		printf("\nHello, dear user! This short manual is for you!\n\n"	
		"To use this program you should enter the following commands:\n"
		"<disk>:/<dir>/<thisprogram.exe> <disk>:/<dir>/<keywords.txt> " 
		"<disk>:/<dir>/<filetoanalyse.txt>\n\n"
		"Complaints and suggestions: d.st.cheremuhin@gmail.com\n\nPress any key.\n\n");
		getch();
		exit (1);
	}
	tmp = Executor( fopen( argv[1], "rt"), p_workTree, tmp, 1); // Файл иходных данных.
	tmp = Executor( fopen( argv[2], "rt"), p_workTree, tmp, 2); // Анализируемый файл.
	printTree(tmp);
	getch();
	return 0;
}

/* КРАТКИЙ МАНУАЛ:
Для запуска программы, из консоли win7 используйте запись типа: 

>>> d:/test/test.exe d:/test/keywords.txt d:/test/program.txt

При необходимости статичных имён, из main() удаляются аргументы argc и argv.
В вызов функции Executor вписываются статичные имена файлов по типу:

>>> tmp = Executor(fopen("keywords.txt", "rt"), p_workTree, tmp, 1);
>>> tmp = Executor(fopen("program.txt", "rt"), p_workTree, tmp, 2);

По функции workTree.
При передаче в workTree модификатора == 1, она переключается в режим "создание дерева".
Текстовый файл просматривается, создаются узлы со словами перечисленными в файле.
При повторе слов в исходном файле счетчики в узлах не изменяются.
При передаче в workTree модификатора == 2, она переключается в режим "чтение дерева".
Текстовый файл просматривается, изменяются счетчики в узлах, новые узлы не создаются.
Модификатор получается из ф-ии Executor, куда передаётся из main.
*/