/*Написать программу, которая строит таблицу встречаемости сим-
волов для произвольного файла, имя которого задаётся в команд-
ной строке. Программа должна выводить на экран таблицу встре-
чаемости, отсортированную по убыванию частоты*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define FNAME "program.txt" // Имя открываемого файла.

struct SYM {
	int code;
	float frequency;
};

typedef struct SYM TSYM;
typedef TSYM * PSYM;

int comp(const void*b, const void*a) {
	return (((PSYM)a)->frequency - ((PSYM)b)->frequency);	
}

int main() {
	FILE *fp;
	int i = 0, c = 0, count = 0, number = 0, symnum[256] = {0};
	PSYM buffer =(PSYM)calloc(256,sizeof(TSYM));
	for(i = 0; i < 256; i++) buffer[i].code = -1;	
	fp = fopen(FNAME,"rt");
	if(!fp) {
		perror(FNAME);
		exit(1);
	}
	while ((c = fgetc(fp)) != EOF) {
		number++;
		for (i = 0; i < 256; i++) {			
			if (buffer[i].code == -1) {
				buffer[i].code = c;
				buffer[i].frequency++;
				count++;
				i++;
				break;
			}
			else if (buffer[i].code == c) {
				buffer[i].frequency++;
				i++;
				break;
			}			
		}		
	}
	fclose(fp);
	printf("Total number of signs: %d\n\n", number);
	qsort(buffer, count, sizeof(TSYM), comp);
	for (i = 0; i<count; i++) buffer[i].frequency = buffer[i].frequency/number;
	for (i = 0, c = 0; i < count; i++) {	
		if (buffer[i].code > 31) {
			printf("  Sym '%c' Code %.3d Freq %f  ", (unsigned char)buffer[i].code, buffer[i].code, buffer[i].frequency);
			c++;
		}
		if ((c)%2 == 0) putchar('\n');
	}
	puts("\n\n");	
	return 0;
}