/*Написать программу, создающую связанный список с записями о
регионах и их кодах в соответствии с содержанием файла данных*/
#include "task01.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main() {
	FILE *fp;
	int count = 0;
	char buf[512], search[10];
	PITEM head, tail, item;
	fp = fopen("fips10_4.csv","rt");
	if(!fp) {
		perror("File fips10_4.csv");
		exit(1);
	}
	fgets(buf, 512, fp);
	while(fgets(buf, 512, fp)){
		if(count == 0){
			head = createList(createName(buf));
			tail = head;
		}
		else tail = addToTail(tail, createName(buf));
		count++;
	}
	fclose(fp);
	printf("Total items: %d\n", countList(head));
	puts("Type the letter code(for example: RU):");
	scanf("%s", &search);
	item = findByName(head, search);
	if(item == NULL) printf("Not found!\n");
	return 0;
}